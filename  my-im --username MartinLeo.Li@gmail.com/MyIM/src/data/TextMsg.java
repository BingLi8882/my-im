package data;

import java.io.Serializable;
/**
 * this class hold the message sent between friends
 * @author Bing Li; Kaidi Xu
 *
 */
public class TextMsg implements Serializable{
	

	private static final long serialVersionUID = 1L;
	private String from;
	private String to;
	private String content;
	/**
	 * the constructor
	 * @param f the sender name
	 * @param t the receiver name
	 * @param c the message
	 */
	public TextMsg(String f,String t,String c){
		from  = f;
		to = t;
		content = c;
	}
	/**
	 * get the sender
	 * @return the sender name
	 */
	
	public String getFrom(){
		return from;
	}
	/**
	 * get the receiver name
	 * @return
	 */
	public String getTo(){
		return to;
	}
	/**
	 * get the message
	 * @return
	 */
	public String getContent(){
		return content;
	}

}
