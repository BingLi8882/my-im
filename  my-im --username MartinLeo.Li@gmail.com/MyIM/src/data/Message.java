package data;

import java.io.Serializable;
/**
 * the message class used for interact with server
 * @author Bing Li; Kaidi Xu
 *
 */
public class Message implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private final int type;
	private	final Object content;
	/**
	 * the constructor
	 * @param t the message type
	 * @param c the content
	 */
	public Message(int t,Object c){
		type = t;
		content = c;
	}
	/**
	 * get the message type
	 * @return a number which is the constant values in the information class
	 */
	public int getType(){
		return type;
	}
	/**
	 * get the content: textmsg, userinfo,friendlist
	 * @return the content
	 */
	public Object getContent(){
		return content;
	}

}
