package data;

import java.io.Serializable;

/**
 * Contain information of MyIM 
 * @author Bing Li; Kaidi Xu
 *
 */
public class Information implements Serializable{

	private static final long serialVersionUID = 1L;
	public static final double version_number = 0.3; // version number
	public static final int server_port_number = 55667; // server port #
	public static final int client_port_number = 45668;
	
	
	public static final int MESSAGE_TYPE_ACK = 0;
	public static final int MESSAGE_TYPE_ERR = 1;
	public static final int MESSAGE_TYPE_LOGIN = 2;
	public static final int MESSAGE_TYPE_REGIST = 3;
	public static final int MESSAGE_TYPE_ADD_FRIEND = 4;
	public static final int MESSAGE_TYPE_DELETE_FRIEND = 5;
	public static final int MESSAGE_TYPE_SENT_TO = 6;
	public static final int MESSAGE_TYPE_USER_INFO = 7;
	
	
	public static final int MESSAGE_TYPE_GROUP_ADD = 9;
	public static final int MESSAGE_TYPE_GROUP_SENT = 10;
	public static final int MESSAGE_TYPE_GROUP_LIST = 11;
	

	
	
}
