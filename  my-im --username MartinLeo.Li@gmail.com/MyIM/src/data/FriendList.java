package data;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
/**
 * the friend or group list
 * @author Bing Li; Kaidi Xu
 *
 */
public class FriendList implements Iterable<String>, Serializable {
	

	private static final long serialVersionUID = 1L;
	private LinkedList<String> list;
	/**
	 * the constructor
	 */
	public FriendList(){
		list = new LinkedList<String>();
	}
	/**
	 * add a new element
	 * @param n the name
	 */
	public void addfriend(String n){
		list.add(n);
	}
	/**
	 * remove an element from the list
	 * @param n the name
	 */
	public void removefriend(String n){
		list.remove(n);
	}
	/**
	 * the iterator
	 */
	public Iterator<String> iterator() {
		
		return list.iterator();
	}
	
}
