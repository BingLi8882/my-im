package data;

import java.io.Serializable;
/**
 * the user info class
 * @author Bing Li; Kaidi Xu
 */
public class UserInfo implements Serializable{

	private static final long serialVersionUID = 1L;
	private String username;
	private String password;
	/**
	 * the constructor
	 * @param n the user name
	 * @param p the password
	 */
	public UserInfo(String n,String p){
		username = n;
		password = p;
		
	}
	/**
	 * get user name
	 * @return the user name
	 */
	public String getUserName(){
		return username;
	}
	/**
	 * get the password
	 * @return the password
	 */
	public String getPassword(){
		return password;
	}
}
