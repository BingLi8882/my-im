package database;

import java.io.Serializable;
/**
 * the structure stores the message information
 * @author kaidix
 *
 */
public class message implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String name, message;
	/**
	 * constructor
	 * @param name the sender's name
	 * @param message the content of the message 
	 */
	public message(String name, String message){
		this.name=name;
		this.message=message;
	}
}
