package database;

import java.io.Serializable;
import java.util.Vector;
/**
 * the group structure which saves group member's name
 * @author kaidix
 *
 */
public class group implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String name;
	Vector<groupMember> mem;
	/**
	 * constructor
	 * @param name the group name
	 * @param owner the owner
	 */
	public group(String name, String owner){
		this.name=name;
		mem = new Vector<groupMember>();
		mem.add(new groupMember(owner,0));
	}
	/**
	 * add a member into the group
	 * @param name the member's name
	 */
	public void addMember(String name){
		for(int i = 0; i < mem.size();i++){
			if(mem.elementAt(i).name.equals(name)){
				return;
			}
		}
		mem.add(new groupMember(name,3));
	}
	/**
	 * remove a member from the group (not used)
	 * @param name the name of the member
	 */
	public void removeMember(String name){
		for(int i = 0; i < mem.size();i++){
			if(mem.elementAt(i).name.equals(name)){
				mem.remove(i);
			}
		}
	}
	/**
	 * change the privilege (not used)
	 * @param name the member's name
	 * @param pri the privilege level
	 * @return true if success
	 */
	public boolean changePriviledge(String name,int pri){
		for(int i = 0 ; i < mem.size();i++){
			if(mem.get(i).name.equals(name)){
				mem.get(i).priviledge=pri;
				return true;
			}
		}
		return false;
	}
	/**
	 * return the member's name in this group
	 * @return a string vector contains member's name
	 */
	public Vector<String> getMembers(){
		Vector<String> tmp = new Vector<String>();
		for(int i = 0 ; i < mem.size();i++){
			tmp.add(mem.get(i).name);
		}
		return tmp;
	}
	/**
	 * get the privilege of a member
	 * @param name the member's name
	 * @return privilege level
	 */
	public int getPriviledge(String name){
		for(int i = 0 ; i < mem.size();i++){
			if(mem.get(i).name.equals(name)){
				return mem.get(i).priviledge;
			}
		}
		return -1;
	}
	
}
