package database;

import java.io.Serializable;
/**
 * group member's info
 * @author kaidix
 *
 */
public class groupMember implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String name;
	int priviledge;
	/**
	 * treat as one person in group for this object
	 * @param name member's name
	 * @param pri privilege level
	 */
	public groupMember(String name, int pri){
		this.name=name;
		priviledge = pri;
	}
}
