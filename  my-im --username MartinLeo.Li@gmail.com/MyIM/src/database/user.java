package database;

import java.io.Serializable;
import java.util.Vector;
/**
 * a structure store user's infomation
 * @author kaidix
 *
 */
public class user implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name,password;
	private Vector<String> friendList;
	private Vector<message> message;
	
	private Vector<message> groupMessage;
	/**
	 * 
	 * @param name user name
	 * @param pwd password
	 */
	public user(String name,String pwd){
		this.name = name;
		this.password = pwd;
		friendList = new Vector<String>();
		message = new Vector<message>();
		groupMessage = new Vector<message>();
	}
	/**
	 * change password
	 * @param pwd the password
	 */
	public void changePwd(String pwd){
		password = pwd;
	}
	/**
	 * get password
	 * @return the password
	 */
	public String getPwd(){
		return password;
	}
	/**
	 * get user's name
	 * @return the user's name
	 */
	public String getUserName(){
		return name;
	}
	/**
	 * try to add a "name" into the friend list
	 * @param name the name to add
	 * @return on name already exist, return -1
	 * 		   on error, return 0;
	 * 		   on success, return 1;
	 */
	public int addFriend(String name){
		for(int i = 0; i < friendList.size();i++){
			if(friendList.elementAt(i).equals(name)){
				return -1;
			}
		}
		if(friendList.add(name)){
			return 1;
		}
		else{
			return 0;
		}
	}
	/**
	 * try to remove a "name" from the friend list
	 * @param name the name to remove
	 * @return on success, return 1
	 * 			on fail (name not found), return 0;
	 */
	public int removeFriend(String name){

		for(int i = 0; i < friendList.size();i++){
			if(friendList.elementAt(i).equals(name)){
				friendList.remove(i);
				return 1;
			}
		}
		return 0;
	
	}
	/**
	 * get someone's friend list
	 * @return the friend list of the user
	 */
	public Vector<String> getFriendList(){
		return friendList;
	}
	/**
	 * 
	 * @return the unread messages of the user
	 */
	public Vector<database.message> getMessage(){
		return message;
	}
	/**
	 * add a new message to this user
	 * @param name the sender
	 * @param message the text
	 */
	public void addMessage(String name,String message){
		this.message.add(new message(name,message));
	}
	/**
	 * remove all messages
	 */
	public void clearMessage(){
		message.clear();
	}
	
	/**
	 * 
	 * @return group messages of this user
	 */
	public Vector<database.message> getGroupMessage(){
		return groupMessage;
	}
	/**
	 * add a new group message
	 * @param name the group name
	 * @param message the text
	 */
	public void addGroupMessage(String name,String message){
		this.groupMessage.add(new message(name,message));
	}
	/**
	 * remove all group message
	 */
	public void clearGroupMessage(){
		groupMessage.clear();
	}
	
}
