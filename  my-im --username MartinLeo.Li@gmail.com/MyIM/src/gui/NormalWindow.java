package gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.*;

import client.*;
import data.*;
/**
 * this is the normal window(the main window)
 * @author Bing Li; Kaidi Xu
 *
 */
public class NormalWindow extends JFrame {

	private static final long serialVersionUID = 1L;

	private int window_height = 600;
	private int window_width = 200;
	private Client client;

	private JButton friend;
	private JButton group;
	private JButton addFriend;
	private JButton addGroup;
	private JScrollPane list;
	private Container window;
	private JLabel userName;
	private JPanel panel;
	/**
	 * the constructor
	 * @param c the reference of the client
	 */
	public NormalWindow(Client c) {
		// label this frame
		super("MyIM " + data.Information.version_number);
		client = c;
		// initial GUI
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent e) {
                ((Window) e.getComponent()).dispose();
                client.close();
            }
        });
		window = getContentPane();
		window.setLayout(null);

		// set position
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension screenSize = kit.getScreenSize();
		int width = (int) screenSize.getWidth();
		int height = (int) screenSize.getHeight();
		setBounds(width / 2 - (window_width / 2), height / 2
				- (window_height / 2), window_width, window_height);
		this.setResizable(false);
		
		userName = new JLabel("UserName: "+c.getUserName());
		userName.setBounds(20, 0, 160, 30);
		window.add(userName);
		
		friend = new JButton("Friend");
		friend.setBounds(20, 30, 80, 30);
		friend.addActionListener(new AbstractAction() {

			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				bfriend();
			}
		});
		window.add(friend);

		group = new JButton("Group");
		group.setBounds(100, 30, 80, 30);
		group.addActionListener(new AbstractAction() {

			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				bgroup();
			}
		});
		window.add(group);

		addFriend = new JButton("Add Friend");
		addFriend.setBounds(20, 500, 160, 30);
		addFriend.addActionListener(new AbstractAction() {

			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				baddfriend();
			}
		});
		window.add(addFriend);

		addGroup = new JButton("Add Group");
		addGroup.setBounds(20, 530, 160, 30);
		addGroup.addActionListener(new AbstractAction() {

			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				baddgroup();
			}
		});
		window.add(addGroup);
		panel = new JPanel();
		panel.setSize(145, 3000);
		panel.setLayout(new GridLayout(100, 1));
		panel.setBackground(Color.white);

		list = new JScrollPane(panel,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		list.setBounds(20, 60, 160, 420);
		list.setVisible(true);
		window.add(list);

		this.updatelist(client.getFriendList(),0);

		this.setVisible(true);
		client.getGroupList();
		client.start();
		

	}
	/**
	 * display friend list
	 */
	private void bfriend() {
		this.updatelist(client.getFriendList(),0);
	}
	/**
	 * display group list
	 */
	private void bgroup() {
		this.updatelist(client.getGroupList(),1);
	}
	/**
	 * open an add friend window
	 */
	private void baddfriend() {
		new AddWindow(client, this, "Friend");
	}
	/**
	 * open an add group window
	 */
	private void baddgroup() {
		new AddWindow(client, this, "Group");
	}
	/**
	 * open a chat window
	 * @param n the name of the friend or group
	 * @param type the target type: 0 for friend;1 for group
	 */
	public void createChat(String n,int type) {
		if (!client.checkChatList(n)) {
			 client.addChat(new ChatWindow(n,client,type));
		}

	}
	/**
	 * update the list which is displaying
	 * @param fl the friend or group list
	 * @param type the type
	 */
	public void updatelist(FriendList fl,int type) {
		panel.removeAll();
		System.out.println("updatefl");
		for (String name : fl) {
			System.out.println(name);
			addList(name,type);
		}
		panel.updateUI();
		
		System.out.println("----------");

	}
	/**
	 * add on element into the list
	 * @param name the name of friend or group
	 * @param type the type
	 */
	public void addList(String name,int type) {
		JButton button = new JButton(name);
		button.setBackground(Color.WHITE);
		button.addActionListener(new ListListener(this, name,type));
		panel.add(button);
		panel.repaint();
		
	}

	/**
	 * the Button listener
	 * @author bingli
	 *
	 */
	private class ListListener implements ActionListener {

		private NormalWindow nw;
		private String name;
		private int type;
		/**
		 * the constructor
		 * @param nw the reference of normal window
		 * @param name the name of target
		 * @param t the type
		 */
		public ListListener(NormalWindow nw, String name,int t) {
			this.nw = nw;
			this.name = name;
			type = t;
		}
		/**
		 * action respond
		 */
		public void actionPerformed(ActionEvent e) {
			nw.createChat(name,type);
		}

	}

}
