package gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;

/**
 * Warning pop window
 * @author Bing Li; Kaidi Xu
 *
 */
public class PopWindow extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	private int window_height = 150;
	private int window_width = 300;

	private JButton ok = new JButton("OK"); 
	private JLabel context;
	
	
	/**
	 * constructor set up frame
	 * @param title frame's label
	 */
	public PopWindow(String text){
				
		// label this frame
		super("Warning");
		
		//initial GUI
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		Container window = getContentPane();
	    window.setLayout(null);
	    //set  position
	    Toolkit kit = Toolkit.getDefaultToolkit();
	    Dimension screenSize = kit.getScreenSize();
	    int width = (int)screenSize.getWidth();
	    int height = (int)screenSize.getHeight();
	    this.setBounds(width/2-(window_width/2), height/2-(window_height/2), window_width, window_height);
	    this.setResizable(false);
	    
		context = new JLabel(text,SwingConstants.CENTER);//set label
		context.setBounds(0,0,window_width,window_height/2);
		//set button
		ok.setBounds(window_width/2-50, window_height/2, 100, 30);
		ok.addActionListener(new AbstractAction(){
			
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e){
				dispose();
			}
		});
		
		window.add(context);
		window.add(ok);
		window.setBackground(Color.lightGray);
	    
	    
	    this.setVisible(true);
		
		
		
	}
	

}
