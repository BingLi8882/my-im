package gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import client.*;
import javax.swing.*;
/**
 * the chat window
 * @author Bing Li; Kaidi Xu
 *
 */
public class ChatWindow extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private int window_height = 500;
	private int window_width = 500;
	JTextArea recv = new JTextArea();
	JTextArea send = new JTextArea();
	JButton sendB = new JButton("send");
	JButton closeB = new JButton("close");
	Client myClient;
	private int type;
	private String from;
	/**
	 * the constructor
	 * @param name the name of target: friend or group
	 * @param c the reference of client
	 * @param t the type
	 */
	public ChatWindow(String name, Client c, int t) {
		// label this frame
		super("Chatting with " + name);
		if(t==1) super.setTitle(name);
		type = t;
		myClient = c;
		from = name;
		recv.setEditable(false);
		send.setEditable(true);

		Container window = getContentPane();
		window.setLayout(null);
		JScrollPane top = new JScrollPane(recv,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		JScrollPane bottom = new JScrollPane(send,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		// initial GUI
		sendB.addActionListener(this);
		closeB.addActionListener(this);
		top.setBounds(20, 20, 460, 270);
		window.add(top);
		bottom.setBounds(20, 300, 460, 80);
		window.add(bottom);
		closeB.setBounds(300, 400, 80, 70);
		sendB.setBounds(400, 400, 80, 70);
		window.add(closeB);
		window.add(sendB);

		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {

			public void windowClosing(WindowEvent e) {
				((Window) e.getComponent()).dispose();
				myClient.removeChat((ChatWindow) e.getComponent());
			}
		});

		// set position
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension screenSize = kit.getScreenSize();
		int width = (int) screenSize.getWidth();
		int height = (int) screenSize.getHeight();
		setBounds(width / 2 - (window_width / 2), height / 2
				- (window_height / 2), window_width, window_height);
		this.setResizable(false);

		this.setVisible(true);
		if (!myClient.addChat(this)) {
			this.dispose();
		}

	}

	
	/**
	 * show message in the window
	 * @param m
	 */
	public void showMsg(data.TextMsg m) {
		String msg = recv.getText() + "\n" + m.getFrom() + " said:\n"
		+ m.getContent() + "\n";
		recv.setText(msg);
	}
	/**
	 * show the group message
	 * @param m
	 */
	public void showGroupMsg(data.TextMsg m){
		String msg = recv.getText() + "\n" + m.getContent() + "\n";
		recv.setText(msg);
	}


	/**
	 * get sender
	 * @return the name of sender
	 */
	public String getFrom() {
		return from;
	}
	/**
	 * the respond of the button action
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource().equals(sendB)) {
			myClient.sendMessage(new data.TextMsg(myClient.getUserName(), from,
					send.getText()), type);
			if (type == 0) {
				String msg = recv.getText() + "\nYou said: \n" + send.getText()
				+ "\n";
				recv.setText(msg);
			}
			send.setText("");
		} else if (e.getSource().equals(closeB)) {
			myClient.removeChat(this);
			this.dispose();
		}

	}
}
