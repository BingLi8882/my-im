package gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;
import client.*;
/**
 * this the Register Window
 * @author Bing Li; Kaidi Xu
 *
 */
public class RegisterWindow extends JFrame {

	private static final long serialVersionUID = 1L;

	private int window_height = 400;
	private int window_width = 400;

	private Client client;

	private JLabel name;
	private JLabel pass;
	private JLabel rep;

	private JTextField nametext;
	private JPasswordField passtext;
	private JPasswordField repass;

	private JButton ok = new JButton("OK");
	private JButton quit = new JButton("quit");
	/**
	 * the constructor
	 * @param c the reference of client
	 */
	public RegisterWindow(Client c) {
		// label this frame
		super("Register");
		client = c;
		// initial GUI
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		Container window = getContentPane();
		window.setLayout(null);

		// set position
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension screenSize = kit.getScreenSize();
		int width = (int) screenSize.getWidth();
		int height = (int) screenSize.getHeight();
		setBounds(width / 2 - (window_width / 2), height / 2
				- (window_height / 2), window_width, window_height);
		this.setResizable(false);
		window.setBackground(Color.lightGray);

		name = new JLabel("UserName:", SwingConstants.RIGHT);
		name.setBounds(20, 50, 100, 50);
		pass = new JLabel("Password:", SwingConstants.RIGHT);
		pass.setBounds(20, 120, 100, 50);
		rep = new JLabel("Re-Password:", SwingConstants.RIGHT);
		rep.setBounds(20, 190, 100, 50);

		window.add(name);
		window.add(pass);
		window.add(rep);

		nametext = new JTextField("", SwingConstants.LEFT);
		nametext.setBounds(120, 60, 200, 30);
		nametext.setBackground(Color.white);
		passtext = new JPasswordField("", SwingConstants.LEFT);
		passtext.setBounds(120, 130, 200, 30);
		passtext.setBackground(Color.white);
		repass = new JPasswordField("", SwingConstants.LEFT);
		repass.setBounds(120, 200, 200, 30);
		repass.setBackground(Color.white);

		window.add(nametext);
		window.add(passtext);
		window.add(repass);

		ok.setBounds(70, 300, 60, 30);
		ok.addActionListener(new AbstractAction() {

			private static final long serialVersionUID = 1L;

			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				if (passtext.getText().equals(repass.getText()) == false) {
					new PopWindow("different password");
				} else {
					if (client.regist(nametext.getText(), passtext.getText())) {
						new PopWindow("Successed");

					} else {
						new PopWindow("Invalid username");

					}
				}
			}
		});

		quit.setBounds(270, 300, 60, 30);
		quit.addActionListener(new AbstractAction() {

			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});

		window.add(ok);
		window.add(quit);

		this.setVisible(true);

	}

}
