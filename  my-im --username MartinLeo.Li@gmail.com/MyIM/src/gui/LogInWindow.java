package gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.*;
import client.*;
/**
 * this is the log in window
 * @author Bing Li; Kaidi Xu
 *
 */
public class LogInWindow extends JFrame {

	private static final long serialVersionUID = 1L;

	private int window_height = 300;
	private int window_width = 400;

	private JLabel name;
	private JLabel pass;

	private JTextField nametext;
	private JPasswordField passtext;

	private JButton ok = new JButton("OK");
	private JButton register = new JButton("register");
	private JButton quit = new JButton("quit");

	private Client client;
	/**
	 * the constructor
	 * @param c the client reference
	 */
	public LogInWindow(Client c) {
		// label this frame
		super("MyIM-Welcome!");
		client = c;
		// initial GUI
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {

			public void windowClosing(WindowEvent e) {
				((Window) e.getComponent()).dispose();
				client.close();
			}
		});

		Container window = getContentPane();
		window.setLayout(null);

		// set position
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension screenSize = kit.getScreenSize();
		int width = (int) screenSize.getWidth();
		int height = (int) screenSize.getHeight();
		setBounds(width / 2 - (window_width / 2), height / 2
				- (window_height / 2), window_width, window_height);
		this.setResizable(false);
		window.setBackground(Color.LIGHT_GRAY);

		name = new JLabel("UserName:", SwingConstants.RIGHT);
		name.setBounds(20, 50, 100, 50);
		pass = new JLabel("Password:", SwingConstants.RIGHT);
		pass.setBounds(20, 120, 100, 50);

		window.add(name);
		window.add(pass);

		nametext = new JTextField("", SwingConstants.LEFT);
		nametext.setBounds(120, 60, 200, 30);
		nametext.setBackground(Color.white);
		passtext = new JPasswordField("", SwingConstants.LEFT);
		passtext.setBounds(120, 130, 200, 30);
		passtext.setBackground(Color.white);

		window.add(nametext);
		window.add(passtext);

		ok.setBounds(70, 200, 60, 30);
		ok.addActionListener(new AbstractAction() {

			private static final long serialVersionUID = 1L;

			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				sent(nametext.getText(), passtext.getText());
			}
		});
		register.setBounds(150, 200, 100, 30);
		register.addActionListener(new AbstractAction() {

			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				regist();
			}
		});
		quit.setBounds(270, 200, 60, 30);
		quit.addActionListener(new AbstractAction() {

			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				client.close();
			}
		});

		window.add(ok);
		window.add(register);
		window.add(quit);

		this.setVisible(true);

	}
	/**
	 * send log in message
	 * @param n the user name
	 * @param p the password
	 */
	private void sent(String n, String p) {
		if (client.login(n, p)) {

			client.creatNormalWindow();
			dispose();
		} else {
			new PopWindow("Incorrect username/password");
		}
	}
	/**
	 * open a new register window
	 */
	private void regist() {
		new RegisterWindow(client);
	}
	/**
	 * test function
	 * @param args
	 */
	public static void main(String[] args) {
		new LogInWindow(new Client());
	}
}
